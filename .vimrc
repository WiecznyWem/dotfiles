filetype plugin indent on
setlocal expandtab
setlocal shiftwidth=4
setlocal softtabstop=4
set nu
set relativenumber
set mouse=a
set cursorline
syntax enable
set background=light
hi TabLineSel ctermfg=DarkGreen ctermbg=White
hi TabLine ctermfg=Grey ctermbg=White
hi TabLineFill ctermfg=White
hi LineNr ctermfg=LightGrey
" WSL yank
let s:clip = '/mnt/c/Windows/System32/clip.exe'  " change this path according to your mount point
if executable(s:clip)
    augroup WSLYank
        autocmd!
        autocmd TextYankPost * if v:event.operator ==# 'y' | call system(s:clip, @0) | endif
    augroup END
endif 
